/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.datastlab3;

/**
 *
 * @author Paweena Chinasri
 */
public class SqrtSolution {
    public static void main(String[] args) {
        int x1 = 4;
        int result1 = mySqrt(x1);
        System.out.println("Output: " + result1);

        int x2 = 8;
        int result2 = mySqrt(x2);
        System.out.println("Output: " + result2);
    }

    public static int mySqrt(int x) {
        if (x == 0 || x == 1) {
            return x;
        }

        long left = 1;
        long right = x / 2; // Sqrt(x) is never greater than x / 2 for x > 1

        while (left <= right) {
            long mid = left + (right - left) / 2;
            long square = mid * mid;

            if (square == x) {
                return (int) mid;
            } else if (square < x) {
                left = mid + 1;
            } else {
                right = mid - 1;
            }
        }

        // At this point, 'right' is the floor value of the square root
        return (int) right;
    }
}
